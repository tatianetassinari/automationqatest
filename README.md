### Project description:

### This project was created to test and validate a sortable table where the rows are randomly numerated from 0 to 5.

### The goal of this challenge is to sort the rows of this table by drag and drop them from the lowest value to the top.

### The project was created using the IntelliJ IDEA Community Edition 2018.2 and Gradle as dependency management in Windows 7 (Please, take in consideration this QA doesn't have access to an Apple Mac Book).

### The project uses the following set of artifacts and their respective versions as defined in the build.gradle:

### JUnit having version 4.12
### java having version 1.7 or more

### Decision about how to handle test data:
### The code verifies each row in a loop and while the String doesn't match it keeps verifying each row. Once the list is ordered correctly (from 0 to 5), it closes the browser and print the list in the console.

### Assumptions
### You have an IDEA compatible with gradle already setup and running in your machine.
### You have the qa-sortable-challenge up and running in your local machine in: http://localhost:3000
### You have chromedriver.exe in your C:

### How to execute the project:
#### After checkout the project to your machine and open it with the IDEA and guarantee is everything ok related to the gradle dependencies, you have two ways to run the tests:
### Option 1: Open the package src/test/java/RunSortableTest, and Run it directly clicking with right button and choosing: Run 'RunSortableTest'
### Option 2: Play diretly in Run/Debug configuration dialog with the option: RunSortableTest in Build project

### Framework:
### The test cases were creted using Selenium with Java.

#### Project Author: Tatiane Tassinari (tatianetassinari85@gmail.com)
