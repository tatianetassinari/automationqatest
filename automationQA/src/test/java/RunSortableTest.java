import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class RunSortableTest {

    static WebDriver driver;

    private static final By INITIAL_LIST = By.cssSelector("#app>ul>li");

    @BeforeClass
    public static void init() {

        System.setProperty("webdriver.chrome.driver", "C://chromedriver.exe");

        // Initialize browser
        //WebDriver driver = new ChromeDriver();
        driver = new ChromeDriver();
        // Open app
        driver.get("localhost:3000");
        // Maximize browser
        driver.manage().window().maximize();

        Assert.assertEquals("Mindera QA Challenge", driver.getTitle());

    }

    @AfterClass
    public static void quit() {
        driver.close();
    }


    @Test
    public void orderListElements() throws InterruptedException {

        List<WebElement> list = driver.findElements(INITIAL_LIST);
        //ArrayList<WebElement> list = (ArrayList<WebElement>) driver.findElements(INITIAL_LIST);

        int tam = list.size();
        System.out.println("The size of this list is: " + tam);


        String firstElement;
        String secondElement;
        String thirdElement;
        String fourthElement;
        String fifthElement;
        String lastElement;


        do {

            firstElement = driver.findElement(By.xpath("//*[@id='app']/ul/li[1]")).getText();
            secondElement = driver.findElement(By.xpath("//*[@id='app']/ul/li[2]")).getText();
            thirdElement = driver.findElement(By.xpath("//*[@id='app']/ul/li[3]")).getText();
            fourthElement = driver.findElement(By.xpath("//*[@id='app']/ul/li[4]")).getText();
            fifthElement = driver.findElement(By.xpath("//*[@id='app']/ul/li[5]")).getText();
            lastElement = driver.findElement(By.xpath("//*[@id='app']/ul/li[6]")).getText();

            for (int j = 1; j < list.size(); j++) {

                WebElement source = driver.findElement(By.xpath(".//*[@id='app']/ul/li[" + j + "]"));
                String getvalue = driver.findElement(By.xpath(".//*[@id='app']/ul/li[" + j + "]")).getText();


                switch (getvalue) {
                    case "Item 0": {
                        Thread.sleep(6000);
                        WebElement dest = driver.findElement(By.xpath("//*[@id='app']/ul/li[1]"));

                        if (dest.getText() != "Item 0") {

                            Actions builder = new Actions(driver);
                            builder.dragAndDrop(source, dest).build().perform();

                        }


                        break;

                    }
                    case "Item 1": {

                        Thread.sleep(6000);

                        WebElement dest = driver.findElement(By.xpath("//*[@id='app']/ul/li[2]"));

                        if (dest.getText() != "Item 1") {

                            Actions builder = new Actions(driver);
                            builder.dragAndDrop(source, dest).build().perform();
                        }

                        break;

                    }
                    case "Item 2": {

                        Thread.sleep(6000);

                        WebElement dest = driver.findElement(By.xpath("//*[@id='app']/ul/li[3]"));

                        if (dest.getText() != "Item 2") {

                            Actions builder = new Actions(driver);
                            builder.dragAndDrop(source, dest).build().perform();
                        }

                        break;

                    }
                    case "Item 3": {

                        Thread.sleep(6000);

                        WebElement dest = driver.findElement(By.xpath("//*[@id='app']/ul/li[4]"));

                        if (dest.getText() != "Item 3") {

                            Actions builder = new Actions(driver);
                            builder.dragAndDrop(source, dest).build().perform();
                        }

                        break;

                    }
                    case "Item 4": {

                        Thread.sleep(6000);

                        WebElement dest = driver.findElement(By.xpath("//*[@id='app']/ul/li[5]"));

                        if (dest.getText() != "Item 4") {

                            Actions builder = new Actions(driver);
                            builder.dragAndDrop(source, dest).build().perform();
                        }

                        break;

                    }
                    case "Item 5": {

                        Thread.sleep(6000);

                        WebElement dest = driver.findElement(By.xpath("//*[@id='app']/ul/li[6]"));

                        if (dest.getText() != "Item 5") {

                            Actions builder = new Actions(driver);
                            builder.dragAndDrop(source, dest).build().perform();
                        }

                        break;

                    }


                }


            }


        } while (!(firstElement.equalsIgnoreCase("Item 0"))
                || !(secondElement.equalsIgnoreCase("Item 1"))
                || !(thirdElement.equalsIgnoreCase("Item 2"))
                || !(fourthElement.equalsIgnoreCase("Item 3"))
                || !(fifthElement.equalsIgnoreCase("Item 4"))
                || !(lastElement.equalsIgnoreCase("Item 5")));


        System.out.println("Updated list: " + "\n" + driver.findElement(By.id("app")).getText());


    }


}

